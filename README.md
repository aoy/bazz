# Bazz
Simple Gtk2 interface to mplayer.
No playlists, just directories.
Bazz uses 3-level directory tree:

    music
        pop 
            author1
              song1.mp3
              song2.mp3
              song3.mp3
            radio
              radio1.pls
              radio2.pls
        dir2
            ...

![](./screenshot.png)

## Install
* Install go-gtk:
`go get github.com/mattn/go-gtk`
* Add strings to $GOPATH/src/github.com/mattn/go-gtk/gtk/gtk.go:

    func (v *MenuItem) GetLabel() string {
        return gostring(C.gtk_menu_item_get_label(MENU_ITEM(v)))
    }

* Install bazz:
`go get bitbucket.org/aoy/bazz`.
* Change path to the music directory - **musDir** in main.go.
* Install bazz:
`go install bitbucket.org/aoy/bazz` 
or 
`cd $GOPATH/src/bitbucket.org/aoy/bazz; go build`.

## Control
**Double click** or **Enter** - play selected file.
**Space** - pause/resume.
**Left arrow** - previous.
**Right arrow** - next.
**PageUp** - move track up.
**PageDown** - move track down.
