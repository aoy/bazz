package main

import (
	"log"
	"os"
	"os/exec"
	"syscall"
)

const (
	idle = iota
	stop
	play
	pause
)

type Context struct {
	actList []string
	curList []string
	proc    *os.Process
	pos     int
	state   int
}

func lg(err error) {
	if err != nil {
		log.Println(err)
	}
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func term(c *Context, dc <-chan bool) {
	if c.proc == nil {
		return
	}
	c.state = stop
	err := c.proc.Signal(syscall.SIGCONT)
	if err != nil {
		lg(err)
		return
	}
	err = c.proc.Signal(syscall.SIGTERM)
	if err != nil {
		lg(err)
		return
	}
	c.state = stop
	<-dc
}

func ext(file string) string {
	for i := len(file) - 1; i >= 0; i-- {
		if file[i] == '.' {
			return file[i+1:]
		}
	}
	return file
}

func pauseResume(c *Context) {
	if c.proc == nil {
		return
	}
	if c.state == play {
		err := c.proc.Signal(syscall.SIGSTOP)
		lg(err)
		c.state = pause
	} else if c.state == pause {
		err := c.proc.Signal(syscall.SIGCONT)
		lg(err)
		c.state = play
	}
}

func playFile(fc <-chan string, pc chan<- *os.Process, dc chan<- bool) {
	var file string
	var com []string
	var attr = &os.ProcAttr{Files: []*os.File{nil, nil, nil}}
	ppath, err := exec.LookPath(player)
	fatal(err)
	var proc *os.Process
	for {
		file = <-fc
		if ext(file) == "pls" {
			com = []string{player, "-@", file}
		} else {
			com = []string{player, file}
		}
		proc, err = os.StartProcess(ppath, com, attr)
		if err != nil {
			lg(err)
			continue
		}
		pc <- proc
		_, err = proc.Wait()
		lg(err)
		dc <- true
	}
}

func playFn(c *Context, fc chan string, sc chan status) {
	sc <- status{title(c.actList[c.pos]), c.pos}
	fc <- c.actList[c.pos]
	c.state = play
}

func listen(ec <-chan event, lc <-chan []string, sc chan status) {
	fc := make(chan string)
	pc := make(chan *os.Process)
	dc := make(chan bool)
	go playFile(fc, pc, dc)

	var e event
	c := &Context{}
	for {
		select {
		case c.curList = <-lc:
		case c.proc = <-pc:
		case <-dc:
			c.proc = nil
			c.state = idle
			if c.pos < len(c.actList)-1 {
				c.pos++
				playFn(c, fc, sc)
			}
		case e = <-ec:
			switch e.code {
			case exit:
				term(c, dc)
				return
			case act:
				term(c, dc)
				c.pos = e.pos
				c.actList = c.curList
				playFn(c, fc, sc)
			case spaceKey:
				pauseResume(c)
			case leftKey:
				term(c, dc)
				c.actList = c.curList
				if c.pos > 0 {
					c.pos--
				}
				playFn(c, fc, sc)
			case rightKey:
				term(c, dc)
				c.actList = c.curList
				if c.pos < len(c.actList)-1 {
					c.pos++
				}
				playFn(c, fc, sc)
			case upKey:
				if e.pos > 0 {
					c.pos = e.pos - 1
				} else {
					c.pos = e.pos
				}
			case downKey:
				if e.pos < len(c.curList)-1 {
					c.pos = e.pos + 1
				} else {
					c.pos = e.pos
				}
			}
		}
	}
}
