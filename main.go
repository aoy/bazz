package main

import (
	"os"
	"strconv"
	"time"
	"unsafe"

	"github.com/mattn/go-gtk/gdk"
	"github.com/mattn/go-gtk/glib"
	"github.com/mattn/go-gtk/gtk"
)

const (
	musDir   = "/home/Stuff/Audio/music"
	icon     = "/usr/share/icons/bazz.png"
	player   = "mpg123"
	xSize    = 300
	ySize    = 450
	defTitle = "Bazz"
)

const (
	pgupKey   = 65365
	pgdownKey = 65366
	enterKey  = 65293
	spaceKey  = 32
	leftKey   = 65361
	rightKey  = 65363
	upKey     = 65362
	downKey   = 65364

	act  = 0
	exit = -1
)

type event struct {
	code int
	pos  int
}

type status struct {
	title string
	pos   int
}

func setList(lc chan []string, dir string,
	tv *gtk.TreeView, str *gtk.ListStore) []string {

	files, err := content(dir)
	fatal(err)
	if len(files) == 0 {
		println("There are no files...")
		return nil
	}
	checkAndCorrect(files, dir)
	paths := make([]string, len(files))
	for i := range files {
		paths[i] = dir + "/" + files[i]
	}
	str.Clear()
	var iter gtk.TreeIter
	for _, v := range files {
		str.Append(&iter)
		str.Set(&iter, 0, noExt(v))
	}
//	tv.SetCursor(gtk.NewTreePathFromString("0"), nil, false)
	lc <- paths
	return paths
}
func getPos(tview *gtk.TreeView) int {
	var tpath *gtk.TreePath
	var column *gtk.TreeViewColumn
	tview.GetCursor(&tpath, &column)
	pos, _ := strconv.Atoi(tpath.String())
	return pos
}

func swapFiles(a, b string) {
	na, ta := split(a) // na - number substring, ta - title substring
	nb, tb := split(b)

	err := os.Rename(a, nb+ta)
	if err != nil {
		lg(err)
		return
	}
	err = os.Rename(b, na+tb)
	if err != nil {
		lg(err)
	}
}

func split(s string) (string, string) {
	var x int
	for i := len(s) - 1; i > 0; i-- {
		if s[i] == '/' {
			x = i
			break
		}
	}
	for i := x; i < len(s); i++ {
		if s[i] == '.' {
			return s[:i], s[i:]
		}
	}
	return "", s
}

func getDir(path string) string {
	for i := len(path) - 1; i > 0; i-- {
		if path[i] == '/' {
			return path[:i]
		}
	}
	return path
}

func main() {
	gtk.Init(&os.Args)

	win := gtk.NewWindow(gtk.WINDOW_TOPLEVEL)
	win.SetTitle(defTitle)
	win.SetIconFromFile(icon)

	tview := gtk.NewTreeView()
	store := gtk.NewListStore(glib.G_TYPE_STRING,
		glib.G_TYPE_STRING)
	tview.SetModel(store)
	tview.AppendColumn(
		gtk.NewTreeViewColumnWithAttributes("",
			gtk.NewCellRendererText(), "text", 0))
	tview.SetHeadersVisible(false)
	tview.SetEvents(5)

	vbox := gtk.NewVBox(false, 0)
	mbar := gtk.NewMenuBar()
	vbox.PackStart(mbar, false, false, 0)
	mainMenu, err := content(musDir)
	fatal(err)

	var list []string
	var lc = make(chan []string)  // list chan
	var sc = make(chan status, 1) // staus chan
	var ec = make(chan event)     // event chan

	for _, v := range mainMenu {
		menu := gtk.NewMenuItemWithLabel(v)
		mbar.Append(menu)
		smenu := gtk.NewMenu()
		menu.SetSubmenu(smenu)
		subMenu, err := content(musDir + "/" + v)
		fatal(err)
		for _, u := range subMenu {
			mitem := gtk.NewMenuItemWithLabel(u)
			mitem.Connect("activate", func() {
				list = setList(lc, musDir+"/"+menu.GetLabel()+
					"/"+mitem.GetLabel(), tview, store)
			})
			smenu.Append(mitem)
		}
	}

	swin := gtk.NewScrolledWindow(nil, nil)
	swin.SetPolicy(3, 1)
	swin.Add(tview)
	vbox.Add(swin)
	win.Add(vbox)
	win.Resize(xSize, ySize)
	win.ShowAll()

	win.Connect("destroy", func() {
		ec <- event{exit, 0}
		gtk.MainQuit()
	})

	var t = time.Now()
	//	var iter1, iter2 gtk.TreeIter
	win.Connect("key-press-event", func(ctx *glib.CallbackContext) {
		arg := ctx.Args(0)
		ev := *(**gdk.EventKey)(unsafe.Pointer(&arg))
		key := ev.Keyval
		//println(key)
		if key != enterKey && key != upKey && key != downKey {
			win.StopEmission("key-press-event")
		}
		if time.Since(t) < 1e8 {
			return
		}
		t = time.Now()
		if key == pgupKey {
			pos := getPos(tview)
			if pos < 1 {
				return
			}
			d := getDir(list[pos])
			swapFiles(list[pos], list[pos-1])
			list = setList(lc, d, tview, store)
			n := strconv.Itoa(pos - 1)
			tview.SetCursor(gtk.NewTreePathFromString(n),
				nil, false)
		}

		if key == pgdownKey {
			pos := getPos(tview)
			if pos > len(list)-2 {
				return
			}
			d := getDir(list[pos])
			swapFiles(list[pos], list[pos+1])
			list = setList(lc, d, tview, store)
			n := strconv.Itoa(pos + 1)
			tview.SetCursor(gtk.NewTreePathFromString(n),
				nil, false)
		}

		ec <- event{int(key), getPos(tview)}
	})

	glib.TimeoutAdd(60, func() bool {
		if len(sc) > 0 {
			st := <-sc
			win.SetTitle(st.title)
			n := strconv.Itoa(st.pos)
			tview.SetCursor(gtk.NewTreePathFromString(n),
				nil, false)
		}
		return true
	})

	tview.Connect("row_activated", func() {
		ec <- event{act, getPos(tview)}
	})

	go listen(ec, lc, sc)

	s, err := content(musDir + "/" + mainMenu[0])
	fatal(err)
	if len(s) != 0 {
		list = setList(lc, musDir+"/"+mainMenu[0]+"/"+s[0], tview, store)
	}

	gtk.Main()
}
