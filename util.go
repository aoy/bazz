package main

import (
	"log"
	"os"
	"sort"
	"strconv"
)

func content(dir string) ([]string, error) {
	d, err := os.Open(dir)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer d.Close()
	s, err := d.Readdirnames(-1)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	sort.Strings(s)
	return s, nil
}

func noExt(s string) string {
	for i := len(s) - 1; i > 0; i-- {
		if s[i] == '.' {
			return s[:i]
		}
	}
	return s
}

func title(s string) string {
	var i, n int
	for i = len(s) - 1; i > 0 && n < 2; i-- {
		if s[i] == '/' {
			n++
		}
	}
	return noExt(s[i+2:])
}

func fastEq(a, b []string) bool {
	if len(a) != len(b) || a[0] != b[0] || a[len(a)-1] != b[len(a)-1] {
		return false
	}
	return true
}

func validName(maxLen, n int, name string) (string, bool) {
	num := number(maxLen, n)
	if name[0] > 57 || name[0] < 48 {
		return num + "." + name, false
	}
	var m int
	for i, v := range name {
		if v == '.' {
			m = i
			break
		}
	}
	for _, v := range name[:m] {
		if v > 57 || v < 48 {
			return num + "." + name, false
		}
	}
	if num != name[:m] {
		return num + name[m:], false
	}
	return name, true
}

func number(maxLen, n int) string {
	x := strconv.Itoa(n + 1)
	for maxLen > len(x) {
		x = "0" + x
	}
	return x
}

func checkAndCorrect(s []string, dir string) {
	var ok bool
	var u string
	var err error
	maxLen := len(strconv.Itoa(len(s)))
	for i, v := range s {
		if u, ok = validName(maxLen, i, v); ok {
			continue
		}
		s[i] = u
		err = os.Rename(dir+"/"+v, dir+"/"+u) // ?
		if err != nil {
			lg(err)
		}
	}
}
